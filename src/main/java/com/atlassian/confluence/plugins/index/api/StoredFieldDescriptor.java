package com.atlassian.confluence.plugins.index.api;

/**
 * Used for non analyzed data.
 */
public final class StoredFieldDescriptor extends FieldDescriptor {
    public StoredFieldDescriptor(String name,
                                 String value) {
        super(name, value, Store.YES, Index.NO);
    }

    @Override
    public <T> T accept(FieldVisitor<T> fieldVisitor) {
        return fieldVisitor.visit(this);
    }
}
