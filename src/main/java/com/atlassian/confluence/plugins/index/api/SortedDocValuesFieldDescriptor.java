package com.atlassian.confluence.plugins.index.api;

/**
 * Stores binary value in a columnar form. The values are sorted, deduplicated and mapped to ordinal before storing.
 */
public final class SortedDocValuesFieldDescriptor extends FieldDescriptor {
    private final byte[] bytesValue;

    public SortedDocValuesFieldDescriptor(String name, byte[] value) {
        super(name, null, Store.SORTED_DOC_VALUES, FieldDescriptor.Index.NO);
        this.bytesValue = value;
    }

    public <T> T accept(FieldVisitor<T> fieldVisitor) {
        return fieldVisitor.visit(this);
    }

    public byte[] bytesValue() {
        return bytesValue;
    }
}
