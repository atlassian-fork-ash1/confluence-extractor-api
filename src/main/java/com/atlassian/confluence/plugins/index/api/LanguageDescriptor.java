package com.atlassian.confluence.plugins.index.api;

import com.atlassian.annotations.ExperimentalApi;

/**
 * Language is used to define an analyzer that is language dependent see {@link LanguageAnalyzerDescriptor}.
 * It must be predefined in Confluence. It is a capability of a product and not something that
 * plugin can change.
 *
 * @since 2.0.7
 */
@ExperimentalApi
public interface LanguageDescriptor {
}
