package com.atlassian.confluence.plugins.index.api;

/**
 * Used for keywords or non analyzed text.
 */
public final class StringFieldDescriptor extends FieldDescriptor {
    public StringFieldDescriptor(String name,
                                 String value,
                                 Store store) {
        super(name, value, store, Index.NOT_ANALYZED);
    }

    @Override
    public <T> T accept(FieldVisitor<T> fieldVisitor) {
        return fieldVisitor.visit(this);
    }
}
