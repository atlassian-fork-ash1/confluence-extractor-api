package com.atlassian.confluence.plugins.index.api;

/**
 * Declares a visitor over the hierarchy of types under {@link FieldDescriptor}.
 *
 * @param <T> the type of the return value of all visit methods in this class
 */
public interface FieldVisitor<T> {
    T visit(FieldDescriptor field);

    T visit(StringFieldDescriptor field);

    T visit(TextFieldDescriptor field);

    T visit(IntFieldDescriptor field);

    T visit(LongFieldDescriptor field);

    T visit(FloatFieldDescriptor field);

    T visit(DoubleFieldDescriptor field);

    T visit(StoredFieldDescriptor field);

    T visit(DocValuesFieldDescriptor field);

    T visit(SortedDocValuesFieldDescriptor field);
}
