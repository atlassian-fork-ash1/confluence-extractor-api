package com.atlassian.confluence.plugins.index.api;

import com.atlassian.annotations.ExperimentalApi;

import java.util.Optional;

/**
 * Provides an {@link AnalyzerDescriptor} that should be used to analyze text on a given {@link LanguageDescriptor} if
 * it is defined.
 *
 * @since 2.0.6
 */
@ExperimentalApi
@FunctionalInterface
public interface AnalyzerDescriptorProvider {
    Optional<AnalyzerDescriptor> getAnalyzer(LanguageDescriptor language);
}
