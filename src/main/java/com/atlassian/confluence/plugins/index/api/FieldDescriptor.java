package com.atlassian.confluence.plugins.index.api;

import com.atlassian.annotations.Internal;

/**
 * Represents an index field.
 * <p>
 * Use one of the specific subclasses of {@link AbstractFieldDescriptor} instead.
 */
@Internal
public class FieldDescriptor extends AbstractFieldDescriptor {
    public FieldDescriptor(String name, String value, Store store, Index index) {
        super(store, name, index, value);
        if (name == null || name.trim().isEmpty())
            throw new IllegalArgumentException("name is required.");
        if (store == null)
            throw new IllegalArgumentException("store is required.");
        if (index == null)
            throw new IllegalArgumentException("index is required.");
    }

    public <T> T accept(FieldVisitor<T> fieldVisitor) {
        return fieldVisitor.visit(this);
    }

    public enum Index {
        NO,
        ANALYZED,
        NOT_ANALYZED
    }

    public enum Store {
        NO,
        YES,
        DOC_VALUES,
        SORTED_DOC_VALUES
    }

}
