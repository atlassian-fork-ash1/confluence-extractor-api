package com.atlassian.confluence.plugins.index.api;

import com.atlassian.annotations.ExperimentalApi;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Objects;
import java.util.Optional;

import static java.util.Objects.requireNonNull;

/**
 * An analyzer is a combination of a {@link TokenizerDescriptor} and some (possibly no) {@link CharFilterDescriptor} and
 * {@link TokenFilterDescriptor} that collective specifies how to break a text into token.
 *
 * @since 2.0.2
 */
@ExperimentalApi
public class AnalyzerDescriptor implements AnalyzerDescriptorProvider {
    private final Collection<CharFilterDescriptor> charFilters;
    private final TokenizerDescriptor tokenizer;
    private final Collection<TokenFilterDescriptor> tokenFilters;

    private AnalyzerDescriptor(Builder builder) {
        this.charFilters = new ArrayList<>(requireNonNull(builder.charFilters, "charFilters"));
        this.tokenizer = requireNonNull(builder.tokenizer, "tokenizer");
        this.tokenFilters = new ArrayList<>(requireNonNull(builder.tokenFilters, "tokenFilter"));
    }

    public Collection<CharFilterDescriptor> getCharFilters() {
        return Collections.unmodifiableCollection(charFilters);
    }

    public TokenizerDescriptor getTokenizer() {
        return tokenizer;
    }

    public Collection<TokenFilterDescriptor> getTokenFilters() {
        return Collections.unmodifiableCollection(tokenFilters);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AnalyzerDescriptor)) return false;
        AnalyzerDescriptor that = (AnalyzerDescriptor) o;
        return Objects.equals(getCharFilters(), that.getCharFilters()) &&
                Objects.equals(getTokenizer(), that.getTokenizer()) &&
                Objects.equals(getTokenFilters(), that.getTokenFilters());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCharFilters(), getTokenizer(), getTokenFilters());
    }

    public static Builder builder(TokenizerDescriptor tokenizer) {
        return new Builder(tokenizer);
    }

    @Override
    public Optional<AnalyzerDescriptor> getAnalyzer(LanguageDescriptor language) {
        return Optional.of(this);
    }

    public static class Builder {
        private Collection<CharFilterDescriptor> charFilters;
        private TokenizerDescriptor tokenizer;
        private Collection<TokenFilterDescriptor> tokenFilters;

        private Builder(TokenizerDescriptor tokenizer) {
            this.charFilters = new ArrayList<>();
            this.tokenizer = requireNonNull(tokenizer);
            this.tokenFilters = new ArrayList<>();
        }

        public Builder charFilter(CharFilterDescriptor charFilter) {
            charFilters.add(charFilter);
            return this;
        }

        public Builder tokenFilter(TokenFilterDescriptor tokenFilter) {
            tokenFilters.add(tokenFilter);
            return this;
        }

        public AnalyzerDescriptor build() {
            return new AnalyzerDescriptor(this);
        }
    }
}
